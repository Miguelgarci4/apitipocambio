DROP TABLE IF EXISTS TIPOCAMBIO;
CREATE TABLE TIPOCAMBIO(
id INT AUTO_INCREMENT PRIMARY KEY,
moneda_de_origen VARCHAR(50) NOT NULL,
moneda_de_destino VARCHAR(50) NOT NULL,
monto_tipo_de_cambio DOUBLE,
tipo_de_cambio DOUBLE,
);

INSERT INTO TIPOCAMBIO (id, moneda_de_origen, moneda_de_destino, monto_tipo_de_cambio, tipo_de_cambio) VALUES (1, 'Soles', 'Dolares', 10.0, '3.7');