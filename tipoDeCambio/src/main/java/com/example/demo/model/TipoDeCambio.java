package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name="tipoDeCambio")
public class TipoDeCambio {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	String monedaDeOrigen;
	String monedaDeDestino;
	double montoTipoDeCambio;
	double tipoDeCambio;
}
