package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.TipoDeCambio;
import com.example.demo.service.TipoDeCambioService;

@RestController
@RequestMapping("/api/tiposdecambio")
public class TipoDeCambioController {
	@Autowired
	private TipoDeCambioService tipoDeCambioService;
	
    @GetMapping(value = "/obtener")
    ResponseEntity<List<TipoDeCambio>> obtenerTodosLosTiposDeCambio() {
        List<TipoDeCambio> tiposDeCambio = tipoDeCambioService.obtenerTodosLosTiposDeCambio();
        return ResponseEntity.ok(tiposDeCambio);
    }
    
    @PostMapping("/guardar")
    public ResponseEntity<String> guardarTipoDeCambio(@RequestBody TipoDeCambio tipoDeCambio) {
        tipoDeCambioService.guardarTipoDeCambio(tipoDeCambio);
        return new ResponseEntity<>("Tipo de cambio guardado", HttpStatus.CREATED);
    }
    
    @PutMapping("/actualizar/{id}")
    public ResponseEntity<String> actualizarTipoDeCambio(@PathVariable int id, @RequestBody TipoDeCambio tipoDeCambioActualizado) {
        try {
            tipoDeCambioService.actualizarTipoDeCambio(id, tipoDeCambioActualizado);
            return ResponseEntity.ok("Tipo de cambio actualizado");
        } catch (Exception e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }
}
