package com.example.demo.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.TipoDeCambio;
import com.example.demo.repository.TipoDeCambioRepository;
import com.example.demo.service.TipoDeCambioService;

@Service
public class TipoDeCambioServiceImpl implements TipoDeCambioService{
	
	@Autowired
	TipoDeCambioRepository tipoDeCambioRepository;

	@Override
	public List<TipoDeCambio> obtenerTodosLosTiposDeCambio() {		
		return tipoDeCambioRepository.findAll();
	}
	
	@Override
    public void guardarTipoDeCambio(TipoDeCambio tipoDeCambio) {        
        tipoDeCambioRepository.save(tipoDeCambio);
    }
    
	@Override
    public void actualizarTipoDeCambio(int id, TipoDeCambio tipoDeCambioActualizado) {
    	 Optional<TipoDeCambio> opcionalTipoDeCambio = tipoDeCambioRepository.findById(id);

         if (opcionalTipoDeCambio.isPresent()) {
             TipoDeCambio tipoDeCambioExistente = opcionalTipoDeCambio.get();
             tipoDeCambioExistente.setMonedaDeOrigen(tipoDeCambioActualizado.getMonedaDeOrigen());
             tipoDeCambioExistente.setMonedaDeDestino(tipoDeCambioActualizado.getMonedaDeDestino());
             tipoDeCambioExistente.setMontoTipoDeCambio(tipoDeCambioActualizado.getMontoTipoDeCambio());
             tipoDeCambioExistente.setTipoDeCambio(tipoDeCambioActualizado.getTipoDeCambio());

             tipoDeCambioRepository.save(tipoDeCambioExistente);
         } else {
             System.out.println("Tipo de cambio no encontrado con id: " + id);
         }
    }

}
