package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.TipoDeCambio;

@Repository
public interface TipoDeCambioRepository extends JpaRepository<TipoDeCambio, Integer>{
	
}
