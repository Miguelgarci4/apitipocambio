package com.example.demo.service;

import java.util.List;

import com.example.demo.model.TipoDeCambio;

public interface TipoDeCambioService {
	public List<TipoDeCambio> obtenerTodosLosTiposDeCambio();
	public void guardarTipoDeCambio(TipoDeCambio tipoDeCambio);
	public void actualizarTipoDeCambio(int id, TipoDeCambio tipoDeCambioActualizado);
}
